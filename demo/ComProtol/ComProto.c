﻿#include "ComProto.h"
#include "bufifo.h"

//--------------------------------------------
//  载重实验串口通讯协议
//  

//---------------- Decode Frame ---------------

static uint8_t EscapFrameBuff[FRAME_MAX_LEN];

int GetEncodedFrameBuff(int fifo_index, uint8_t ** getBuffpt)
{
    static int FrameStartFlag = 0;
    static int FrameLens = 0;
    static uint8_t EncodedFrameBuff[FRAME_MAX_LEN];
	uint8_t data = 0;
    int retLen= 0;
	fifo_t *pfifo = (fifo_t *)fifo_index;

    if( !getBuffpt ) return 0;
    *getBuffpt = NULL;

    while ( fifo_read(fifo_index, &data) )
    {
        if (data == FRAME_START)
        {
            //start to receive a new frame
            FrameStartFlag = 1;
            FrameLens = 0;
        }

        if (FrameStartFlag)
        {
            EncodedFrameBuff[FrameLens++] = data;
             if (data == FRAME_END)
            {
                 if (FrameLens < FRAME_MIN_LEN) {
                     return 0;
                 }
                 retLen = FrameLens;
 				 FrameStartFlag = 0;
				 FrameLens = 0;
                 *getBuffpt = EncodedFrameBuff;
				 return retLen;
			}
		}
		if( FrameLens > FRAME_MAX_LEN )
        {
			FrameStartFlag = 0;
			FrameLens = 0;
            return 0;
        }
	}
	return 0;
}

int DecodeFrameBufferMsg(uint8_t * framebuff,  int fbufflen)
{
    uint8_t data;
    uint16_t i, crc16;
	uint16_t parseLen = 0;

	if( !framebuff || fbufflen < FRAME_MIN_LEN ) 
		return 0;

    for(i = 0; i < fbufflen; i++)
    {
        if(framebuff[i] == ESCAPE_BYTE)
        {
            if( ++i >= fbufflen ) 
				return -1; //UnEscape Error
            switch(framebuff[i])
            {
            case ESCAPE_BYTE_OF_START:
                data = FRAME_START;
                break;
            case ESCAPE_BYTE_OF_END:
                data = FRAME_END;
                break;
            case ESCAPE_BYTE_OF_ESCAPE:
                data = ESCAPE_BYTE;
                break;
            default:
				//UnEscape Btype Error
                return -1;
            }
        }
        else
        {
            data = framebuff[i];
        }

        EscapFrameBuff[parseLen++] = data;
        if(parseLen >= FRAME_MAX_LEN)
        {
            return -1;
        }
    }

	/* only include the CRC */
	if(parseLen < FRAME_MIN_LEN)
	{
		printf("DecodeError: Frame Len is too short. \r\n");
		return -1;
	}
	crc16 = MakeCRC16(&EscapFrameBuff[1], parseLen-4);
	if( crc16 != * ((uint16_t*)&EscapFrameBuff[parseLen-3]) )
	{
        printf("DecodeError: Frame CRC Error. \r\n");
		return -1;
	}
    memcpy( framebuff, EscapFrameBuff, parseLen );
	return parseLen;
}

//---------------- Encode Frame ---------------

int EncodeFrameBufferMsg(uint8_t * ptdata,  int datalen)
{
    int16_t i;
	int16_t escLen = 0;
	
	if( !ptdata || datalen ==0){
		return -1;
	}

	EscapFrameBuff[escLen++] = FRAME_START;
    for(i = 0; i < datalen; i++)
    {
        switch (ptdata[i])
		{
		case ESCAPE_BYTE:
			EscapFrameBuff[escLen++] = ESCAPE_BYTE;
			EscapFrameBuff[escLen++] = ESCAPE_BYTE_OF_ESCAPE;
			break;
		case FRAME_START:
			EscapFrameBuff[escLen++] = ESCAPE_BYTE;
			EscapFrameBuff[escLen++] = ESCAPE_BYTE_OF_START;
			break;
		case FRAME_END:
			EscapFrameBuff[escLen++] = ESCAPE_BYTE;
			EscapFrameBuff[escLen++] = ESCAPE_BYTE_OF_END;
			break;
		default:
			EscapFrameBuff[escLen++] = ptdata[i];
			break;
		}

        if(escLen >= FRAME_MAX_LEN)
        {
            printf("EcodeError: Frame too long. \r\n");
            return -1;
        }
    }
	EscapFrameBuff[escLen++] = FRAME_END;
	memcpy( ptdata, EscapFrameBuff, escLen);
	return escLen;
}

//Cmd Frame Fill
int FillCmdFrametoSend(uint16_t CmdID, uint8_t * data, int datalen, uint8_t *outFrameBuff, int buffsize)
{
	int16_t crc16 = 0;
	int frameLen = 0;
	if( !data || !outFrameBuff || datalen <=0 || buffsize <=0 || buffsize<= datalen+8){
		return -1;
	}

	stFrameCmdHeader * ptStCmdFrame = (stFrameCmdHeader *) outFrameBuff;
	ptStCmdFrame->command = CmdID;
	ptStCmdFrame->cmdlen = datalen + 4;
	memcpy( ptStCmdFrame->data, data, datalen);
	crc16 = MakeCRC16( outFrameBuff, datalen+4);
	*((uint16_t *) &ptStCmdFrame->data[datalen]) = crc16;

	// *((uint16_t *) &outFrameBuff[0]) = CmdID;
	// *((uint16_t *) &outFrameBuff[0]) = datalen+4;
	// memcpy( &outFrameBuff[4], data, datalen);
	// crc16 = CRC16( &outFrameBuff[0], datalen+4);
	// *((uint16_t *) &outFrameBuff[datalen+4]) = crc16;
	
	frameLen = EncodeFrameBufferMsg(outFrameBuff, datalen+6);
	return frameLen;
}



//================================Modbus============================

uint16_t MakeCRC16(uint8_t *pData, uint32_t plen)
{
    uint16_t u16CRC =0xFFFF;
    uint16_t siRet = 0;
    int i , j;
    if( !pData || plen <=0)
    {
        return 0;
    }
    for(i =0; i < plen; i++)
    {
        u16CRC ^=(uint16_t)(pData[i]);
        for(j =0; j <=7; j++)
        {
            if(u16CRC &0x0001)
            {
                u16CRC =(u16CRC >>1)^0xA001;
            }
            else
            {
                u16CRC = u16CRC >>1;
            }
        }
    }
    siRet =u16CRC;
    //siRet =(u16CRC &0x00FF)<<8;
    //siRet |= u16CRC>>8;
    return siRet;
}

//Modbus Protocl 

