﻿// ConsoleApplication1.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <stdio.h>
#include "bufifo.h"
#include "ringfifo.h"
#include "ComProto.h"

struct pt_ringbuf ReadRingbuf;
struct pt_ringbuf WriteRingbuf;
static uint8_t gFrameBuff[FRAME_MAX_LEN] = { 0 };
static uint8_t fillFrameBuff[FRAME_MAX_LEN] = { 0 };
struct ringbuf ringfifoRecv[RING_BUFF_LEN];
struct ringbuf ringfifoSend[RING_BUFF_LEN];
RingBuffers RingBufferRecv;
RingBuffers RingBufferSend;
int readfifo = 0;


int SendCommandMsg(uint16_t CmdID, uint8_t * data, int datalen, int ftype)
{
	int len;
	len = FillCmdFrametoSend(0x1250, data, datalen, fillFrameBuff, FRAME_MAX_LEN);
	if (len > 0) {
		len = ringput(&RingBufferSend, fillFrameBuff, len, ftype);
		return len;
	}
	
	return 0;
}


int main()
{

	uint8_t* getFrameBuff = NULL;
	int len, DecLen;
	uint8_t data, i;

	ringmalloc(&RingBufferRecv, ringfifoRecv, RING_ITEMS_MAX);
	ringmalloc(&RingBufferSend, ringfifoSend, RING_ITEMS_MAX);
	readfifo = fifo_initial();
	///////测试FIFO缓冲区读写操作///////
	printf("\r\n///////测试FIFO缓冲区读写操作///////\r\n");
	fifo_writelen(readfifo, "@@@@@@@@@@123456789012345678901234567890@@@@@@@@", 48);
	while (fifo_read(readfifo, &data)) printf("%c",(char)data); printf("\r\n");
	fifo_writelen(readfifo, "@@@@@@@@@@123456789012345678901234567890", 40);
	i = 1; while (i++ <= 5 && fifo_read(readfifo, &data)) printf("%c", (char)data); printf("\r\n");
	i = 1; while (i++ <= 5 && fifo_read(readfifo, &data)) printf("%c", (char)data); printf("\r\n");
	i = 1; while (i++ <= 5 && fifo_read(readfifo, &data)) printf("%c", (char)data); printf("\r\n");
	fifo_writelen(readfifo, "@@@@@@@@@123456789", 18);
	i = 1; while (i++ <= 5 && fifo_read(readfifo, &data)) printf("%c", (char)data); printf("\r\n");
	fifo_writelen(readfifo, "@012345", 7);
	i = 1; while (i++ <= 5 && fifo_read(readfifo, &data)) printf("%c", (char)data); printf("\r\n");
	i = 1; while (i++ <= 5 && fifo_read(readfifo, &data)) printf("%c", (char)data); printf("\r\n");
	fifo_writelen(readfifo, "@01234567890", 12);
	i = 1; while (i++ <= 30 && fifo_read(readfifo, &data)) printf("%c", (char)data); printf("\r\n");
	i = 1; while (i++ <= 30 && fifo_read(readfifo, &data)) printf("%c", (char)data); printf("\r\n");
	while (fifo_read(readfifo, &data)) printf("%c", (char)data); printf("\r\n");
	fifo_writelen(readfifo, "123456789012345678901234567890", 30);
	while (fifo_read(readfifo, &data)) printf("%c", (char)data); printf("\r\n");

	///////测试FIFO缓冲区 + 存放协议帧（构造指令编码）///////
	printf("\r\n///////测试FIFO缓冲区 + 存放协议帧（构造指令编码）///////\r\n");
	fifo_writelen(readfifo, "#1111111111111111111111111111@", 30);
	len = FillCmdFrametoSend(0x1250, (uint8_t*)"1234567890123456789012345678901234567890testcode", 48, gFrameBuff, FRAME_MAX_LEN);
	fifo_writelen(readfifo, gFrameBuff, len);
	i = 0; while (i++ < len) printf("%c", (char)gFrameBuff[i]); printf("\r\n");
	fifo_writelen(readfifo, "\x77\x77\x55\x55\x5c\x5c\x5e\x5e\x5d\x5d", 10);
	len = FillCmdFrametoSend(0x1250, (uint8_t*)"1234567890\x7e\x5e\x7d\x5d\x7c\x5c-89012345678901234567890testcode", 48, gFrameBuff, FRAME_MAX_LEN);
	fifo_writelen(readfifo, gFrameBuff, len);
	i = 0; while (i++ < len) printf("%c", (char)gFrameBuff[i]); printf("\r\n");

	///////测试FIFO缓冲区 + 读取协议帧 + 指令解码///////
	printf("\r\n///////测试FIFO缓冲区 + 读取协议帧 + 指令解码///////\r\n");
	len = GetEncodedFrameBuff(readfifo, &getFrameBuff);
	i = 0; while (i < len) printf("%c", (char)getFrameBuff[i++]); printf("\r\n");
	len = DecodeFrameBufferMsg(getFrameBuff, len);
	i = 0; while (i < len) printf("%c", (char)getFrameBuff[i++]); printf("\r\n");
	ringput(&RingBufferRecv, getFrameBuff, len, 0);
	len = GetEncodedFrameBuff(readfifo, &getFrameBuff);
	i = 0; while (i < len) printf("%02x ", getFrameBuff[i++]); printf("\r\n");
	len = DecodeFrameBufferMsg(getFrameBuff, len);
	i = 0; while (i < len) printf("%02x ", getFrameBuff[i++]); printf("\r\n");
	ringput(&RingBufferRecv, getFrameBuff, len, 0);

	while ((len = ringget(&RingBufferRecv, &ReadRingbuf)) > 0) {
		//ExecMsgCmd();
	}

	//Send Test CmdFrame

	{	//---- Test ---- Send Init Status Data
		stWzInitStatusNotify packetData = {
			.status = 2, 
			.weight = 1250,
			.battery =99,
		};
		SendCommandMsg(CMDID_INIT, &packetData, sizeof(stWzInitStatusNotify), 0);
	}

	while (ringget(&RingBufferSend, &WriteRingbuf) > 0) {
		//if (len > 0)
			//Rs485WriteMsg(WriteRingbuf.buffer, WriteRingbuf.size);
		printf("-------------Send Rs485-------------\r\n");
		len = WriteRingbuf.size;
		i = 0; while (i < len) printf("%02x ", (uint8_t)WriteRingbuf.buffer[i++]); printf("\r\n");
	}

	//NoBrockRead
	//enable_RxTx(WZCOM_PIN_CS, 0);
	//usleep(500000);
	//len = Rs485ReadBuffer(H11S_ReadBuf);
	if (len > 0) {
		fifo_writelen(readfifo, gFrameBuff, len);
		do {
			len = GetEncodedFrameBuff(readfifo, &getFrameBuff);
			if (len > 0 && getFrameBuff) {
				//Debug FrameBuff: getFrameBuff

				DecLen = DecodeFrameBufferMsg(getFrameBuff, len);
				if (DecLen > 0) {
					//Debug DecodeFrameBuff: getFrameBuff
					ringput(&RingBufferRecv, getFrameBuff, DecLen, 0);
				}
			}
		} while (len > 0);
	}
	//enable_RxTx(WZCOM_PIN_CS, 1);
	getchar();
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
