#ifndef ___BUF_FIFO_H__
#define ___BUF_FIFO_H__

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdint.h"
#include "stdbool.h"


typedef struct {
  uint8_t* data;
  uint32_t isize;
  uint32_t isum;
  uint32_t wpos;
  uint32_t rpos;
  bool is_full;
} fifo_t;

extern int fifo_initial(void);
extern bool fifo_write(int fifo_index, uint8_t *data);
extern bool fifo_read(int fifo_index, uint8_t *data);
extern bool fifo_writelen(int fifo_index, uint8_t *databuff, int len);
int fifo_writeable_item_count(int fifo_index);

#endif //___BUF_FIFO_H__



