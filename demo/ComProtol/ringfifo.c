﻿/*ringbuf .c*/

#include<stdio.h>
#include<ctype.h>
#include <stdlib.h>
#include <string.h>
#include "ringfifo.h"

#define NMAX 32

/* 环形缓冲区的地址编号计算函数，如果到达唤醒缓冲区的尾部，将绕回到头部。
环形缓冲区的有效地址编号为：0到(NMAX-1)
*/
void ringmalloc(RingBuffers* ptRingBuff, struct ringbuf* rings, int ringsize)
{
    int i;
    if( !ptRingBuff || !rings ) return;

    for(i =0; i<ringsize; i++)
    {
        //ringfifo[i].buffer = malloc(size);
        rings[i].size = 0;
        rings[i].frame_type = 0;
    }
    ptRingBuff->ringbufs = rings;
    ptRingBuff->MaxSize = ringsize;
    ptRingBuff->iput = 0;   // 环形缓冲区的当前放入位置
    ptRingBuff->iget = 0;   // 缓冲区的当前取出位置
    ptRingBuff->num = 0;      // 环形缓冲区中的元素总数量 
} 

/**************************************************************************************************
**
**
**
**************************************************************************************************/
void ringreset(RingBuffers *ptRingBuff)
{
    if( !ptRingBuff ) return;
    ptRingBuff->iput = 0; /* 环形缓冲区的当前放入位置 */
    ptRingBuff->iget = 0; /* 缓冲区的当前取出位置 */
    ptRingBuff->num = 0; /* 环形缓冲区中的元素总数量 */
}

/**************************************************************************************************
**
**
**
**************************************************************************************************/
void ringfree(RingBuffers* ptRingBuff)
{
    int i;
    printf("begin free mem\n");
    if( !ptRingBuff ) return;

    for(i =0; i<ptRingBuff->MaxSize; i++)
    {
       // printf("FREE FIFO INFO:idx:%d,len:%d,ptr:%x\n",i,ringfifo[i].size,(int)(ringfifo[i].buffer));
        //free(ptRingBuff->ringfifo[i].buffer);
        ptRingBuff->ringbufs[i].size = 0;
    }
}

/**************************************************************************************************
**
**
**
**************************************************************************************************/
int addring(RingBuffers* ptRingBuff, emIndexTpye itype )
{
    if( !ptRingBuff ) return 0;
    switch (itype)
    {
    case GET_INDEX:
        ptRingBuff->iget ++;
        if (ptRingBuff->iget >= ptRingBuff->MaxSize)
            ptRingBuff->iget = 0;
            
        return ptRingBuff->iget;
        break;
    default:
        ptRingBuff->iput ++;
        if (ptRingBuff->iput >= ptRingBuff->MaxSize)
            ptRingBuff->iput = 0;

        return ptRingBuff->iput;
        break;
    }
    return -1;
}

/**************************************************************************************************
**
**
**
**************************************************************************************************/
/* 从环形缓冲区中取一个元素 */

int ringget(RingBuffers* ptRingBuff, struct pt_ringbuf* getinfo)
{
    int Pos;
    if( !ptRingBuff || !getinfo ) return 0;
    if(ptRingBuff->num>0)
    {
        Pos = ptRingBuff->iget;
        addring(ptRingBuff, GET_INDEX);
        ptRingBuff->num--;
        getinfo->buffer = (ptRingBuff->ringbufs[Pos].buffer);
        getinfo->frame_type = ptRingBuff->ringbufs[Pos].frame_type;
        getinfo->size = ptRingBuff->ringbufs[Pos].size;
        printf("Get FIFO INFO:idx:%d,len:%d,ptr:%x,type:%d\n",Pos,getinfo->size,(int)(getinfo->buffer),getinfo->frame_type);
        return 1;
    }
    else
    {
        //printf("Buffer is empty\n");
        return 0;
    }
}

/**************************************************************************************************
**
**
**
**************************************************************************************************/
/* 向环形缓冲区中放入一个元素*/
int ringput(RingBuffers* ptRingBuff, unsigned char *buffer,int size,int ftype)
{
    if( !ptRingBuff || !buffer || size<=0 || size > RING_BUFF_LEN ) return 0;

    if( ptRingBuff->num < ptRingBuff->MaxSize )
    {
        memcpy(ptRingBuff->ringbufs[ptRingBuff->iput].buffer, buffer, size);
        ptRingBuff->ringbufs[ptRingBuff->iput].size= size;
        ptRingBuff->ringbufs[ptRingBuff->iput].frame_type = ftype;
        printf("Put FIFO INFO:idx:%d,len:%d,ptr:%x,type:%d\n",ptRingBuff->iput, ptRingBuff->ringbufs[ptRingBuff->iput].size, (int)(ptRingBuff->ringbufs[ptRingBuff->iput].buffer), ptRingBuff->ringbufs[ptRingBuff->iput].frame_type);
        addring(ptRingBuff, PUT_INDEX);
        ptRingBuff->num++;
        return size;
    }
    else
    {
        printf("Buffer is full\n");
    }
    return 0;
}
