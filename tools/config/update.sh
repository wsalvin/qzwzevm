#! /bin/sh
if [ ! -d "/home/root/update" ]; then
 exit -1
fi

if [ ! -d "/run/update" ]; then
 exit -1
fi

cd /run/update

if [ -f "rc.local" ]; then
  chmod 755 rc.local
  cp rc.local /etc/rc.local
fi

if [ -f "inittab" ]; then
  cp inittab /etc/inittab
  chmod 644 /etc/inittab
fi

if [ -f "elm.sh" ]; then
  chmod 755 elm.sh
  cp elm.sh /home/root/
fi

if [ -f "elm" ]; then
  cp /home/root/elm /home/root/update/elm.prv
  chmod 755 elm
  mv elm /home/root/elm
fi

if [ -f "elmdevid" ]; then
  chmod 755 elmdevid
  mv elmdevid /home/root/
fi

if [ -f "elmclean.sh" ]; then
  chmod 755 elmclean.sh
  mv elmclean.sh /home/root/
fi

if [ -f "update.sh" ]; then
  chmod 755 update.sh
  cp update.sh /home/root/
fi

if [ -f "elmupdate.sh" ]; then
  chmod 755 elmupdate.sh
  mv elmupdate.sh /home/root/
fi

if [ -f "telnetd" ]; then
  chmod 755 telnetd
  mv telnetd /home/root/
fi

if [ -f "wget" ]; then
  chmod 755 wget
  mv wget /home/root/
fi

if [ -f "busybox" ]; then
  chmod 755 busybox
  mv busybox /home/root
fi

if [ -f "get" ]; then
  chmod 755 get
  mv get /home/root/
fi

if [ -f "8188eu.ko" ]; then
  mv 8188eu.ko /sbin/8188eu.ko
fi

if [ -f "hostapd" ]; then
  chmod 755 hostapd
  mv hostapd /sbin/hostapd
fi

if [ -f "hostapd.conf" ]; then
  mv hostapd.conf /etc/hostapd.conf
fi

if [ -f "udhcpd" ]; then
  chmod 755 udhcpd
  mv udhcpd /sbin/udhcpd
fi

if [ -f "udhcpd.conf" ]; then
  mv udhcpd.conf /etc/udhcpd.conf
fi

if [ -f "runfront.sh" ]; then
  chmod 755 runfront.sh
fi

if [ -f "runbehind.sh" ]; then
  chmod 755 runbehind.sh
fi

if [ -f "testRTSPClient" ]; then
  chmod 755 testRTSPClient
  mv testRTSPClient /home/root/
fi

if [ -f "spidev.ko" ]; then
  chmod 644 spidev.ko
  mv spidev.ko /home/root/
fi

if [ -f "mpu6050.ko" ]; then
  chmod 644 mpu6050.ko
  mv mpu6050.ko /home/root/
fi

if [ -f "mpu6500.ko" ]; then
  chmod 644 mpu6500.ko
  mv mpu6500.ko /home/root/
fi

if [ -f "mpu6500.ko.Beta3" ]; then
  chmod 644 mpu6500.ko.Beta3
  mv mpu6500.ko.Beta3 /home/root/
fi

if [ -f "mpu6500.ko.Beta2" ]; then
  chmod 644 mpu6500.ko.Beta2
  mv mpu6500.ko.Beta2 /home/root/
fi

if [ -f "qzwzevm" ]; then
  chmod 755 qzwzevm
  mv qzwzevm /home/root/
fi

if [ -f "qzwzevm.baktty6" ]; then
  chmod 755 qzwzevm.baktty6
  mv qzwzevm.baktty6/home/root/
fi

if [ -f "qzwzevm.bakttycp" ]; then
  chmod 755 qzwzevm.bakttycp
  mv qzwzevm.bakttycp/home/root/
fi
killall qzwzevm

echo [Update] Files Updated OK.
echo [Update] Files Updated OK. >> /var/volatile/clean.log
echo [Update] Files Updated OK. >> /home/root/clean.log
 

