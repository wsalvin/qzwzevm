﻿/*
*********************************************************************************************************
*
*	模块名称 : EwzCode头文件 
*	文件名称 : includes.h
*	版    本 : V1.0
*
*	修改记录 :
*		版本号    日期        作者     说明
*
*********************************************************************************************************
*/

#ifndef  __INCLUDES_H__
#define  __INCLUDES_H__

/*
*********************************************************************************************************
*                                         标准库
*********************************************************************************************************
*/

#include  <stdarg.h>
#include  <stdio.h>
#include  <stdlib.h>
#include  <math.h>
#include  <string.h>
#include <pthread.h>
#include "typedef.h"

/*
*********************************************************************************************************
*                                        APP DEFInE
*********************************************************************************************************
*/
#define EWZ_VERSION_MAJOR		2
#define EWZ_VERSION_MINOR		4
#define EWZ_VERSION_BUILD		86
#define EWZ_GetSystemVersion(ma, min, build)	( ((ma&0xFF)<<24) | ((min&0xFF)<<16) | (build&0xFFFF) )
#define EWZ_SYSTEM_VERSION		EWZ_GetSystemVersion(EWZ_VERSION_MAJOR, EWZ_VERSION_MINOR, EWZ_VERSION_BUILD)

#define EWZ_HARDWARE_MAJOR		1
#define EWZ_HARDWARE_MINOR		0
#define EWZ_GetHardwareVersion(ma, min)	( ((ma&0xFFFF)<<16) | (min&0xFFFF) )
#define EWZ_HARDWARE_VERSION 	EWZ_GetHardwareVersion(EWZ_HARDWARE_MAJOR, EWZ_HARDWARE_MINOR)

#define MASK_TESTFRAME	0x01
#define MASK_OMITFRAME	0x02
#define MASK_LORAFRAME	0x04

#define MASK_BUTTFILT	0x09
#define MASK_NOBUTTWK	0x08
#define MASK_MANULTEST	0x06	//(0x02 manul only, 0x04 manul+auto)
#define MASK_MANULONLY	0x02
	

#define EWZ_DIR_UP 		0x04													//电梯运行方向 向上
#define EWZ_DIR_DOWN 	0x08													//电梯运行方向 向下
#define EWZ_DIR_STOP 	0x00 


/*
------------------------------------------------------------------------------------------------------------------------
*                                                     TIME OPTIONS
------------------------------------------------------------------------------------------------------------------------
*/

#define  OS_OPT_TIME_DLY                    DEF_BIT_NONE
#define  OS_OPT_TIME_TIMEOUT                ((unsigned  short)DEF_BIT_01)
#define  OS_OPT_TIME_MATCH                  ((unsigned  short)DEF_BIT_02)
#define  OS_OPT_TIME_PERIODIC               ((unsigned  short)DEF_BIT_03)

/* ------------------- BIT DEFINES -------------------- */
#define  DEF_BIT_NONE                                   0x00u
#define  DEF_BIT_00                                     0x01u
#define  DEF_BIT_01                                     0x02u
#define  DEF_BIT_02                                     0x04u
#define  DEF_BIT_03                                     0x08u
#define  DEF_BIT_04                                     0x10u
#define  DEF_BIT_05                                     0x20u
#define  DEF_BIT_06                                     0x40u
#define  DEF_BIT_07                                     0x80u
#define  DEF_BIT_08                                   0x0100u
#define  DEF_BIT_09                                   0x0200u
#define  DEF_BIT_10                                   0x0400u
#define  DEF_BIT_11                                   0x0800u
#define  DEF_BIT_12                                   0x1000u
#define  DEF_BIT_13                                   0x2000u
#define  DEF_BIT_14                                   0x4000u
#define  DEF_BIT_15                                   0x8000u



#ifndef MAIN_EXE
	//Recommanded For All Modules
	#define LogPrintf(...) 	EwzPrintfExt( 1, __VA_ARGS__ )
#else
	//For NetDrv Module
	#define LogPrintf(...) 	printf( __VA_ARGS__ )
#endif

extern int SetSystemTime(char *dt);
extern void  EwzPrintfExt(int log, char *format, ...);
extern unsigned long GetTickofSecCount();
extern uint8_t checkAlarmStat(void);
extern void notifyPowerStat(char flg);
extern void App_PrintfTime(void);
extern long int GetSysTimeStamp(void);
extern long int MakeSysTimeStamp(void);
extern void updateElmStatus(void);
extern int CloseLogFile(void);
extern void ResetAccMainSencor(uint8_t en);
extern void EwzFastInit(void);
extern void EwzForceInit(void);
extern void EwzStopInit(char type);
extern void EwzForceStop(void);
extern void EwzChatAlarmOk(void);
extern void EwzVwJumpDetect(int Count, int VppCount, int VAccHight);
extern uint8_t isPeopleSen(void);
extern void Elm_SetLoraMode(uint8_t en);
extern void ReSetFLoorSen(void);
extern int shSystem(const char *cmdstring);
extern u_short EwzSetRoomTemp(u_short val , u_char lev);
extern void EwzDelAccelData(u_char lev);
extern void initBendConfigData(void);
extern void netStatusUpdate(void);
extern uint32_t EwzGetAlarmBits(void);
extern void EwzUpdateServerMode(void);
extern u_char EwzGetRunDirection(void);
extern void EwzSetStartFloor(u_char sFloor);
extern int16_t EwziGetCurrentFloor(void);
extern uint32_t EwzGetAlarmStat(void);
extern int Elm_GetSecurAlarm(void);

//Auto Sensor Detect
extern void ResetPeopleSensorDetect(void);
extern void ResetTemperSensorDetect(void);
extern void ResetDoorCloseDDetect(void);
extern void ResetDoorOpeneDDetect(void);
extern void ResetFloorSensorDetect(void);
extern void DoorCloseSensorDetect(void);
extern void DoorOpeneSensorDetect(void);
extern void FloorSensorDetect(void);
//WZStart/Stop
extern void EwzManuForceStart(void);
extern void EwzManuForceStop(void);
extern int  EwzManuStopCheck(void);


#endif

/***************************** 安富莱电子 www.armfly.com (END OF FILE) *********************************/
