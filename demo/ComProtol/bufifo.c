#include "bufifo.h"

#define FIFO_BUFF_SIZE      2048
fifo_t gReadingFifo;
static char sReadingBuff[FIFO_BUFF_SIZE]; 
int fifo_initial(void)
{
    fifo_t *pfifo = &gReadingFifo;
    if(pfifo){
        //fifo_t *pfifo = (fifo_t *)malloc(sizeof(fifo_t));
        pfifo->isum = FIFO_BUFF_SIZE;
        pfifo->isize = 1;   //NoUse
        pfifo->wpos = 0;
        pfifo->rpos = 0;
        pfifo->is_full = false;
        pfifo->data = (uint8_t *)sReadingBuff;
        return (int) pfifo;
    }

    return 0;
}
 
 
/* void fifo_delete(int fifo_index)
{
    fifo_t *pfifo = (fifo_t *)fifo_index;
    if (pfifo){
        free(pfifo->data);
        pfifo->data = NULL;
        free(pfifo);
        pfifo = NULL;
    }
} */
 
bool fifo_writeable(int fifo_index)
{
    fifo_t *pfifo = (fifo_t *)fifo_index;
    if (!pfifo) return false;
    return !pfifo->is_full;
}
 
bool fifo_write(int fifo_index, uint8_t *data)
{
    fifo_t *pfifo = (fifo_t *)fifo_index;
    if (!pfifo || pfifo->is_full)
    {
        return false;
    }
 
    //memcpy(pfifo->data + pfifo->wpos * pfifo -> isize, data, pfifo->isize);
    pfifo->data[pfifo->wpos] = *data;
    pfifo->wpos++;
 
    if (pfifo->wpos >= pfifo->isum)
    {
        pfifo->wpos = 0;
    }
    if (pfifo->wpos == pfifo->rpos)
    {
        pfifo->is_full = true;
    }
 
    return true;
}
 
//New: Add chars to fifoBuff
bool fifo_writelen(int fifo_index, uint8_t *databuff, int len)
{
    fifo_t *pfifo = (fifo_t *)fifo_index;
    int leftLen;

    if (!pfifo || pfifo->is_full || len<=0){
        return false;
    }
    else {
        leftLen = fifo_writeable_item_count(fifo_index);
        if (leftLen < len) return false;
    }
 
    leftLen = pfifo->isum - pfifo->wpos;
    if( pfifo->wpos >= pfifo->rpos && leftLen < len ){
        memcpy(pfifo->data + pfifo->wpos, databuff, leftLen);
        memcpy(pfifo->data, databuff+leftLen, len - leftLen);
        pfifo->wpos = len - leftLen;
    }
    else{
        memcpy(pfifo->data + pfifo->wpos, databuff, len);
        pfifo->wpos += len;
    }

    // memcpy(pfifo->data + pfifo->wpos * pfifo -> isize, databuff, pfifo->isize);
    // pfifo->wpos++;
 
    if (pfifo->wpos >= pfifo->isum)
    {
        pfifo->wpos = 0;
    }
    if (pfifo->wpos == pfifo->rpos)
    {
        pfifo->is_full = true;
    }
 
    return true;
}
 
bool fifo_readable(int fifo_index)
{
    fifo_t *pfifo = (fifo_t *)fifo_index;
    if (!pfifo || (pfifo->wpos == pfifo->rpos && !pfifo->is_full))
    {
        return false;
    }
    return true;
}
 
bool fifo_read(int fifo_index, uint8_t *data)
{
    fifo_t *pfifo = (fifo_t *)fifo_index;
    if (!pfifo || (pfifo->wpos == pfifo->rpos && !pfifo->is_full))
    {
        return false;
    }
 
    //memcpy(data, pfifo->data + pfifo->rpos * pfifo->isize, pfifo->isize);
    *data = pfifo->data[pfifo->rpos];
    pfifo->rpos++;
    if (pfifo->rpos >= pfifo->isum)
    {
        pfifo->rpos = 0;
    }
    pfifo->is_full = false;
    return true;
}
 
int fifo_readable_item_count(int fifo_index)
{
    fifo_t *pfifo = (fifo_t *)fifo_index;
    if (!pfifo) return 0;
    if (pfifo->is_full)
    {
        return pfifo->isum;
    }
    else
    {
        return (pfifo->isum + pfifo->wpos - pfifo->rpos) % pfifo->isum;
    }
}
 

 
int fifo_writeable_item_count(int fifo_index)
{
    fifo_t *pfifo = (fifo_t *)fifo_index;

    if (!pfifo || pfifo->is_full)
    {
        return 0;
    }
    else
    {
        if (pfifo->wpos == pfifo->rpos)
        {
            return pfifo->isum;
        }
        else
        {
            return (pfifo->isum + pfifo->rpos - pfifo->wpos) % pfifo->isum;
        }
    }
}
 