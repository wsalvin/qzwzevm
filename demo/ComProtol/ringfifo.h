﻿#ifndef __RING_FIFO_H_
#define __RING_FIFO_H_

#define RING_BUFF_LEN   256
#define RING_ITEMS_MAX  32

struct pt_ringbuf {
    unsigned char *buffer;
    int frame_type;
    int size;
};

struct ringbuf {
    unsigned char buffer[RING_BUFF_LEN];
    int frame_type;
    int size;
};

typedef struct RingBuffType {
    int iput; /* 环形缓冲区的当前放入位置 */
    int iget; /* 缓冲区的当前取出位置 */
    int num; /* 环形缓冲区中的元素总数量 */ 
    int MaxSize;  
    struct ringbuf * ringbufs;
} RingBuffers;

typedef enum  { GET_INDEX = 0, PUT_INDEX = 1 } emIndexTpye;

extern int ringget(RingBuffers* ptRingBuff, struct pt_ringbuf*getinfo);
extern int ringput(RingBuffers* ptRingBuff, unsigned char *buffer, int size, int ftype);
extern void ringmalloc(RingBuffers* ptRingBuff, struct ringbuf* rings, int ringsize);
int addring(RingBuffers* ptRingBuff, emIndexTpye itype );
void ringfree(RingBuffers* ptRingBuff);
void ringreset(RingBuffers* ptRingBuff);

#endif 