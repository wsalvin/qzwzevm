﻿#ifndef ___COM_PROTO_H__
#define ___COM_PROTO_H__

#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"

#pragma pack(1)

#define FRAME_PACKET        220   
#define FRAME_START         0x7E  //frame Start Flag
#define FRAME_END           0x7C  //frame End Flag

#define FRAME_HEADER_SIZE 4
#define FRAME_START_SIZE  1
#define FRAME_END_SIZE    1
#define FRAME_CRC_SIZE    2  
#define FRAME_MIN_LEN  (FRAME_START_SIZE + FRAME_HEADER_SIZE + FRAME_CRC_SIZE + FRAME_END_SIZE)
#define FRAME_MAX_LEN  (FRAME_START_SIZE + FRAME_END_SIZE + FRAME_CRC_SIZE + FRAME_PACKET) 


#define ESCAPE_BYTE                   0x7D
#define ESCAPE_BYTE_OF_START          0x5E
#define ESCAPE_BYTE_OF_END            0x5C
#define ESCAPE_BYTE_OF_ESCAPE         0x5D



/**************************************************************************************************
**		Protocl Struct & Define
**************************************************************************************************/
#define CMDID_PARAM                   0x1250
#define CMDID_INIT                    0x1251
#define CMDID_START                   0x1252
#define CMDID_RESUT                   0x1253
#define CMDID_STATU                   0x1255

typedef struct  _StructFrameCmdType_
{
	/* cmd */
	uint16_t command;
	uint16_t cmdlen;
	uint8_t data[0];
} stFrameCmdHeader;


//---------- command:0x1250
typedef struct
{
	/* Packet Data */
	int32_t method;		//100：设置配置参数指令
} stWzParamDataReq;

typedef struct  
{
	/* Packet Data */
	int32_t method;		//101：设置配置参数指令
	uint32_t weight;	//额定载荷Kg 
	uint32_t ratedSpeed;//额定速率 如：16000 表示 1.6m/s
	uint32_t reserv1;	//楼层总数（预留）
	uint32_t reserv2;		//楼层高度（预留）
	char reserv3[20];	//测试编号 如："WZ001"（预留）
} stWzParamDataResp;


//---------- command:0x1251
typedef struct
{
	/* Packet Data */
	int32_t status;		//0：未就绪
						//1: 就绪态
						//2：自检态
						//3：故障态
						//4：运行态
	uint32_t weight;	//当前额定载荷 （预留）
	uint8_t battery;	//电池电量%*100（预留）
} stWzInitStatusNotify;


//---------- command:0x1252
typedef struct
{
	/* Packet Data */
	int32_t status;		//0：未就绪
						//1: 就绪态
						//2：自检态
						//3：故障态
						//4：运行态
	uint32_t weight;	//当前额定载荷	（预留）
	uint8_t battery;	//电池电量%*100 （预留） 
} stWzStartingNotify;


//---------- command:0x1253
typedef struct
{
	/* Packet Data */
	int32_t status;		//0：未就绪
						//1: 就绪态
						//2：自检态
						//3：故障态
						//4：运行态
	uint32_t weight;	//当前额定载荷
	uint8_t  battery;	//电池电量%*100  如：80 表示%80*100
	int32_t  reserv1;	//提升高度	预留功能
	int32_t speed;		//匀速阶段运行速度 * 10000	必须
	uint32_t A95VibX;	//X轴A95峰值 * 10000（保留4位小数）
	uint32_t A95VibY;	//Y轴A95峰值 * 10000
	uint32_t A95VibZ;	//Z轴A95峰值 * 10000
} stWzEndResultNotify;


//---------- command:0x1255
typedef struct
{
	/* Packet Data */
	int32_t speed;		//当前  速度*10000 
	int32_t curAccel;	//当前加速度*10000（预留）
} stWzRunStatus;




#pragma pack()

extern uint16_t MakeCRC16(uint8_t* pData, uint32_t plen);
extern int GetEncodedFrameBuff(int fifo_index, uint8_t ** getBuffpt);
extern int DecodeFrameBufferMsg(uint8_t * fbuff,  int fbufflen);
extern int FillCmdFrametoSend(uint16_t CmdID, uint8_t * data, int datalen, uint8_t *outFrameBuff, int buffsize);

extern int SendCommandMsg(uint16_t CmdID, uint8_t * data, int datalen, int ftype);
#endif //___COM_PROTO_H__


