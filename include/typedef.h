
#ifndef  __TYPEDEF_H__
#define  __TYPEDEF_H__



/* exact-width signed integer types */
typedef   signed          char int8_t;
typedef   signed         short int16_t;
typedef   signed           int int32_t;
typedef   signed     long long int64_t;

    /* exact-width unsigned integer types */
typedef unsigned          char uint8_t;
typedef unsigned         short uint16_t;
typedef unsigned           int uint32_t;
typedef unsigned     long long uint64_t;

typedef int32_t  s32;
typedef int16_t  s16;
typedef int8_t   s8;

typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t  u8;

/*
------------------------------------------------------------------------------------------------------------------------
*                                                      
------------------------------------------------------------------------------------------------------------------------
*/
	
	typedef enum {FALSE = 0, TRUE = !FALSE} bool;
	typedef enum {RESET = 0, SET = !RESET} FlagStatus, ITStatus, BitStatus, BitAction;
	
	typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;
	#define IS_FUNCTIONAL_STATE(STATE) (((STATE) == DISABLE) || ((STATE) == ENABLE))
	typedef enum {ERROR = 0, SUCCESS = !ERROR} ErrorStatus;

#endif

