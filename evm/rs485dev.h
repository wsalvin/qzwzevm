
#ifndef __RS485_DEV_H__
#define __RS485_DEV_H__

#include "typedef.h"


#define	WZCOM_DEV 	"/dev/ttymxc6cp"
#define WZCOM_PIN_CS		110

int  Rs485ReadBuffer(uint8_t * outbuff, int size);
void Rs485WriteMsg(uint8_t * inputbuff, int bufflen);
int InitRs485Port();
int enable_RxTx(int pin, int value);

#endif /* __RS485_DEV_H__ */
