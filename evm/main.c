
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/route.h>
#include <net/if.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include <asm/types.h>
#include <sys/signal.h>
#include <unistd.h>
#include <net/route.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/file.h>
#include <arpa/inet.h>
#include <netinet/in_systm.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <ctype.h>
#include <netdb.h>
#include <pthread.h>

#include "EwzCore.h"
#include "typedef.h"
#include "EwzTimeEvent.h"
#include "gpio.h"


#include "bufifo.h"
#include "ringfifo.h"
#include "ComProto.h"
#include "rs485dev.h"

static uint8_t gFrameBuff[FRAME_MAX_LEN] = { 0 };
static uint8_t fillFrameBuff[FRAME_MAX_LEN] = { 0 };
struct pt_ringbuf ReadRingbuf;
struct pt_ringbuf WriteRingbuf;
struct ringbuf ringfifoRecv[RING_BUFF_LEN];
struct ringbuf ringfifoSend[RING_BUFF_LEN];
RingBuffers RingBufferRecv;
RingBuffers RingBufferSend;
int readfifo = 0;
stWzParamDataResp gWzParamData = {0};


void AppCommuTask(void)
{
	uint8_t* getFrameBuff = NULL;
	int len;
	uint8_t data, i;
	stFrameCmdHeader * ptHeader = NULL;

	ringmalloc(&RingBufferRecv, ringfifoRecv, RING_ITEMS_MAX);
	ringmalloc(&RingBufferSend, ringfifoSend, RING_ITEMS_MAX);
	readfifo = fifo_initial();
	InitRs485Port();
	enable_RxTx(WZCOM_PIN_CS, 1);
	usleep(500000);

	while(1){
			//Process Decoded CMD FRAME of RECV / Save Data
			while ((len = ringget(&RingBufferRecv, &ReadRingbuf)) > 0) {
				LogPrintf("***ringget Read Frame, ptr:%x size:%d\r\n", (int) ReadRingbuf.buffer, ReadRingbuf.size);
				ptHeader = (stFrameCmdHeader * )&ReadRingbuf.buffer[1];
				LogPrintf("***Command id: 0x%04x, len:%d \r\n", ptHeader->command,  ptHeader->cmdlen);
				if(ptHeader->command == 0x1250){
					gWzParamData = *( (stWzParamDataResp*) ptHeader->data);
					LogPrintf("===== gWzParamData =====\r\n");
					LogPrintf("ParamData.method: %d\r\n", gWzParamData.method);
					LogPrintf("ParamData.weight: %dkg\r\n", gWzParamData.weight);
					LogPrintf("ParamData.RSpeed: %d\r\n", gWzParamData.ratedSpeed);
					LogPrintf("ParamData.reserv1: %d\r\n", gWzParamData.reserv1);
					LogPrintf("ParamData.reserv2: %d\r\n", gWzParamData.reserv2);
					LogPrintf("ParamData.reserv3: \"%s\"\r\n", gWzParamData.reserv3);
				}
			}
			//Sending Encoded CMD FRAME of SEND
			while (ringget(&RingBufferSend, &WriteRingbuf) > 0) {
				ptHeader = (stFrameCmdHeader * )&WriteRingbuf.buffer[1];
				LogPrintf("***Command id: 0x%04x, len:%d \r\n", ptHeader->command,  ptHeader->cmdlen);
				Rs485WriteMsg(WriteRingbuf.buffer, WriteRingbuf.size);

				LogPrintf("-------------Send Rs485-------------\r\n");
				len = WriteRingbuf.size;
				i = 0; while (i < len) LogPrintf("%02x ", (uint8_t)WriteRingbuf.buffer[i++]); LogPrintf("\r\n");
				usleep(20000);
			}

			//NoBrockRead
			usleep(30000);
			enable_RxTx(WZCOM_PIN_CS, 0);
			usleep(30000);
			i = 0;
			do{ //do Reading while
				len = Rs485ReadBuffer(gFrameBuff, FRAME_MAX_LEN);
				if (len > 0) {
					if(len>1){
						LogPrintf("****************Rs485-Reading*****************\r\n");
						i = 0; while (i < len) LogPrintf("%02x ", gFrameBuff[i++]); LogPrintf("\r\n");
					}
					fifo_writelen(readfifo, gFrameBuff, len);
					i = 1;
				}
			}while(len);
			if (i) {
				//do Encoding Data while
				do {
					len = GetEncodedFrameBuff(readfifo, &getFrameBuff);
					if (len > 0 && getFrameBuff) {
						i = 0; while (i < len) LogPrintf("%02x ", getFrameBuff[i++]); LogPrintf("\r\n");
						LogPrintf("****** GetEncodedFrame: ******\r\n");
						//Debug FrameBuff: getFrameBuff

						len = DecodeFrameBufferMsg(getFrameBuff, len);
						LogPrintf("****** DencodedFrame: ******r\n");
						i = 0; while (i < len) LogPrintf("%02x ", getFrameBuff[i++]); LogPrintf("\r\n");
						if (len > 0) {
							//Debug DecodeFrameBuff: getFrameBuff
							ringput(&RingBufferRecv, getFrameBuff, len, 0);
						}
					}
				} while (len > 0);
			}
			enable_RxTx(WZCOM_PIN_CS, 1);
	}//whileing
}

int SendCommandMsg(uint16_t CmdID, uint8_t * data, int datalen, int ftype)
{
	int len;
	len = FillCmdFrametoSend(CmdID, data, datalen, fillFrameBuff, FRAME_MAX_LEN);
	if (len > 0) {
		len = ringput(&RingBufferSend, fillFrameBuff, len, ftype);
		return len;
	}
	
	return 0;
}


/*
*********************************************************************************************************
*	ScheduleInit
*	功能说明: 创建应用任务线程
*	
*********************************************************************************************************
*/

extern void AppCommuTask(void);
extern void Task_IOEventTask(void);
extern void Task_TimerAlarm(void);
int AppScheduleInit()
{
    
    pthread_t thread_TimerAlarm=0;
    pthread_t thread_AppRs485Comm=0;
	pthread_t thread_IOEventTask=0;

    pthread_create(&thread_AppRs485Comm,NULL,AppCommuTask,NULL);
    pthread_create(&thread_TimerAlarm,NULL,Task_TimerAlarm,NULL);
	pthread_create(&thread_IOEventTask,NULL,Task_IOEventTask,NULL);

    return 0;
}

/*
*********************************************************************************************************
*	main
*	功能说明: 标准C程序入口
*********************************************************************************************************
*/

int main(int argc, char* argv[])
{
	int bMode = 0;
	int bDevtype = 1;
	int bBtFilter = 1;
	int bPrint = 1;
	int bOmit = 1;
	int bTest = 0;
	printf("QuanZhou WZEvm System starting ...\r\n");
	printf("Version: %08X \r\n", EWZ_SYSTEM_VERSION);

	loadFiles();

	if(argc>1)
	{
		u8 readChar;
		sscanf(argv[1], "%d", &bDevtype);
		if(argc>2){
			sscanf(argv[2], "%d", &bBtFilter);
			bMode = bBtFilter&MASK_MANULTEST;
			printf("bManutest=%d \n", bMode);
			bBtFilter = bBtFilter&MASK_BUTTFILT;
			printf("ButterFilter=%d \n", bBtFilter);
		}
		if(argc>3){
			sscanf(argv[3], "%d", &readChar);
			bOmit = readChar&MASK_OMITFRAME;
			bTest = readChar&MASK_TESTFRAME;
			bPrint = readChar&MASK_LORAFRAME;
			printf("bOmitFlame=%d \n", bOmit);
			printf("bTestFlame=%d \n", bTest);
			printf("bDebugPrint=%d \n", bPrint);
		}
	}

	printf("devType=%d \n", bDevtype);
	EwzCoreInit(bDevtype, bBtFilter, bPrint, bMode );
	AppScheduleInit();
	
	if(bMode){
		EwzManulProcess(bMode);
	}
	else{
		ResetAccMainSencor(1);
		while(1)
		{
			sleep(1);
		}
	}
	return 0;
}

