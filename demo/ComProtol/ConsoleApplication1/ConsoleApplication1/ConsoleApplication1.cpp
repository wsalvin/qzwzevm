﻿// ConsoleApplication1.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include "bufifo.h"
#include "ringfifo.h"
#include "ComProto.h"

pt_ringbuf ringFrameRead;
pt_ringbuf ringFrameWrite;
static uint8_t aFrameBuff[FRAME_MAX_LEN] = { 0 };
struct ringbuf ringfifoRecv[RING_BUFF_LEN];
struct ringbuf ringfifoSend[RING_BUFF_LEN];
RingBuffers RingBufferRecv;
RingBuffers RingBufferSend;
int readfifo = 0;



int main()
{

	uint8_t* getFrameBuff = NULL;
	int ret, len, DecLen;

	ringmalloc(&RingBufferRecv, ringfifoRecv, RING_ITEMS_MAX);
	ringmalloc(&RingBufferSend, ringfifoSend, RING_ITEMS_MAX);
	readfifo = fifo_initial();

	while ((len = ringget(&RingBufferRecv, &ringFrameRead)) > 0) {
		//ExecMsgCmd();
	}

	//Send Test CmdFrame

	len = FillCmdFrametoSend(0x1250, (uint8_t*)"1234567890123456789012345678901234567890testcode", 48, aFrameBuff, FRAME_MAX_LEN);
	if (len > 0) {
		//Debug EncodeFrameBuff:
		ringput(&RingBufferSend, aFrameBuff, len, 0);
	}

	while (len = ringget(&RingBufferSend, &ringFrameWrite) > 0) {
		//if (len > 0)
			//Rs485WriteMsg(ringFrameWrite.buffer, ringFrameWrite.size);
	}

	//NoBrockRead
	//enable_RxTx(WZCOM_PIN_CS, 0);
	//usleep(500000);
	//len = Rs485ReadBuffer(H11S_ReadBuf);
	if (len > 0) {
		fifo_writelen(readfifo, aFrameBuff, len);
		do {
			len = GetEncodedFrameBuff(readfifo, &getFrameBuff);
			if (len > 0 && getFrameBuff) {
				//Debug FrameBuff: getFrameBuff

				DecLen = DecodeFrameBufferMsg(getFrameBuff, len);
				if (DecLen > 0) {
					//Debug DecodeFrameBuff: getFrameBuff
					ringput(&RingBufferRecv, getFrameBuff, DecLen, 0);
				}
			}
		} while (len > 0);
	}
	//enable_RxTx(WZCOM_PIN_CS, 1);
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门使用技巧: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
