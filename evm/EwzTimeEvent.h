﻿
#ifndef __EWZ_TIMERALARM_H
#define __EWZ_TIMERALARM_H

#include  <stdio.h>

#define	 TIMEOUT_ALARM_STOPDOORCLOSE	65	//8 Lora版本需要更大延迟
#define	 TIMEOUT_ALARM_STOPTRAPPING 	10	
#define	 TIMEOUT_ALARM_DOORCLOSENORUM	30	//15
#define	 TIMEOUT_ALARM_TRAPPING_PEOPLE	60	
#define	 TIMEOUT_ALARM_DOOROPENRUN		5	//15
#define  SENSOR_PEOPLE_IOV				0	//人体感应IO信号
#define	 TIMEOUT_OVERHAUL_EXIST			246
#define	 TIMEOUT_STILL_FLOORSEN			6	//10s 平层信号超时
#define	 TIMEOUT_STOPPING_FLOORSEN		4	//4s 平层信号超时
#define	 TIMEOUT_PUSHBUTTON_RELEASE		2	//4s 平层信号超时
#define	 TIMEOUT_PEOPLE_SENSOR_TEST		86400	
#define	 TIMEOUT_TEMPER_SENSOR_TEST		86400	
#define	 TIMERCOUNT_DOORCLOSED_TEST		3	
#define	 TIMERCOUNT_DOOROPENED_TEST		3	
#define	 TIMERCOUNT_FLOORSENSR_TEST		3	
#define	 TIMER_IO_SAFETYLOOP_EVNET		5
#define  TIMECOUNT_SIDERMOVE_SPEED		5
#define  TIMEROUT_APPSETTING_SEC		150
#define  TIMERCOUNT_SPEEDV_TEST			60

extern int32_t gTimerPushRelease;
extern int32_t gCountPushSwitch;

//检测是否有人，1：有人，0：无人

int shSystem(const char *cmdstring);

extern void SetEvmInitStatus(int32_t status);
extern void SetEvmStartingStatus(int32_t status);
extern void SetEvmRunningStatus(int32_t status);
extern void SetEvmEndStatus(int32_t status);
extern void SetEvmGetResultNotify(int32_t status);

void SendWzStartNotify(int32_t status);
void SendWzParamRequest(void);
void SendWzSpeedStatus(int32_t speed);
void SendWzResultNotify(void);
void SendWzInitStatus(int32_t status);


#define EWZ_STATE_STILL 	 				0x00          //电梯静止
#define EWZ_STATE_SPEED_UP_START 	 		0x01          //开始加速
#define EWZ_STATE_SPEED_UP_HOLD 			0x02		  //匀加速
#define EWZ_STATE_SPEED_HOLD 				0x04          //速度保持
#define EWZ_STATE_SPEED_DOWN_START 		0x05          //开始减速
#define EWZ_STATE_SPEED_DOWN_HOLD 		0x06		  //匀减速
#define EWZ_STATE_SPEED_DOWN_END 		0x08					
#define EWZ_STATE_INIT_READY			0x09


extern int32_t getCurrentSpeed(void);
extern void EwzManuForceStart(void);
extern void EwzManuForceStop(void);
extern int EwzCoreInit(int iDevtype, int bBtFilter, int bDebug, int bMode );
extern void EwzManulProcess(int type);

#pragma pack(1)
typedef struct  {
	u_int   RunCount;	
	int32_t  LiftHeight;
	u_int	MaxSpeed;
	u_int	MinSpeed;	
	u_int	RateSpeed;
	int32_t	MaxAccel; 
	int32_t BrakeDistance;
	int32_t BrakeSpeed;	
	int16_t TargetFloor;
	int16_t BeginFloor;	
	int16_t CurrentFloor;
	u_int   RunTicks;
	int32_t BrakeAccel;	
	int32_t MaxBrakeAcc;
	
	u_int MaxVibX;
	u_int MaxVibY;	
	u_int MaxVibZ;		
	
	u_int A95VibX;	
	u_int A95VibY;	
	u_int A95VibZ;
		
} EWZStatistics;

#pragma pack()

#endif

