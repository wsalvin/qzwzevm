#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <errno.h>

#include "EwzCore.h"
#include "typedef.h"
#include "EwzTimeEvent.h"
#include "ComProto.h"

int32_t gTimerPushRelease = -1;
extern stWzParamDataResp gWzParamData;
//===================================================================
//	 Running Event  CallBack
int32_t gEvmInitStatus = 0;
int32_t gEvmStartingStatus = -1;
int32_t gEvmRunningStatus = 0;
int32_t gEvmEndStatus = 0;
int32_t gEvmGetResultNotify = 0;
EWZStatistics EvmStatistics = {0};

/**************************************************************************
 *   Core 事件/状态回调接口
 *
 *************************************************************************/
// 0:CloseReset, 1：ResetingEnd, 2: Reseting
void SetEvmInitStatus(int32_t status) { gEvmInitStatus = status; }
//-1:UnActive, 9: InitReady 0: Idel, 1:Starting, 4:Holding, 5:Downing, 8:End
void SetEvmStartingStatus(int32_t status) { gEvmStartingStatus = status; }
// 00:电梯静止 ,01:开始加速, 04:速度保持 ,05:开始减速
void SetEvmRunningStatus(int32_t status)
{
	gEvmRunningStatus = status;
	LogPrintf("//-------------------------------------------------------------------- DBG---.Start(%d).RunStatus(%d)\r\n", gEvmStartingStatus, status);
}
// 00:Start, 01:End
void SetEvmEndStatus(int32_t status) { gEvmEndStatus = status; }
// 00:NoResult, 01:Result Data OK
void SetEvmGetResultNotify(int32_t status)
{
	gEvmGetResultNotify = status;
	EwzGetStatisticsResult(&EvmStatistics);
}

int shSystem(const char *cmdstring)
{
	pid_t pid;
	int status = 0;
	if (cmdstring == NULL)
	{
		return (1);
	}
	if ((pid = fork()) < 0)
	{
		status = -1;
	}
	else if (pid == 0)
	{
		execl("/bin/sh", "sh", "-c", cmdstring, (char *)0);
		_exit(127);
	}
	else
	{
		while (waitpid(pid, &status, 0) < 0)
		{
			if (errno != EINTR)
			{
				status = -1;
				break;
			}
		}
	}
	return (status);
}

void Task_TimerAlarm(void)
{
	uint32_t err;
	uint16_t sCount = 0;

	// //---- TEST ---- Send Init Status
	// SendWzStartNotify(2);
	// usleep(500000);

	//---- TEST ---- Request stWzParamDataReq
	usleep(500000);
	SendWzParamRequest();

	while (1)
	{
		usleep(500000);

		if ((++sCount % 5 == 0))
		{
			// ResetAccMainSencor(1);
			if (sCount >= 1000)
				sCount = 0;
		}

		if (gTimerPushRelease > 0)
		{
			if( gTimerPushRelease == TIMEOUT_PUSHBUTTON_RELEASE-1 )
				SetEvmInitStatus(5);
			gTimerPushRelease--;
		}
		else if (gTimerPushRelease == 0)
		{
			LogPrintf("---- BUTTON RELEASE----\r\n");
			gTimerPushRelease = -1;
			EwzManuForceStop();
			ResetAccMainSencor(1);
		}

		//------------ 0.5 Sec perior -------------

		// //---- TEST ---- Send RunStatus
		// SendWzSpeedStatus(sCount);
		if (gEvmStartingStatus > EWZ_STATE_STILL && gEvmStartingStatus < EWZ_STATE_SPEED_DOWN_END)
			SendWzSpeedStatus(sCount);

		if (gEvmStartingStatus == EWZ_STATE_INIT_READY)
		{
			gEvmStartingStatus = EWZ_STATE_STILL;
			SendWzInitStatus(4); // WorkingRun
		}
		else if (gEvmStartingStatus == EWZ_STATE_SPEED_UP_START)
		{
			gEvmStartingStatus = EWZ_STATE_SPEED_UP_HOLD;
			SendWzStartNotify(1);
		}

		if (gEvmGetResultNotify == 1)
		{ //---- TEST ---- Send Result
			gEvmGetResultNotify = 0;
			SendWzResultNotify();
		}

		if (gEvmInitStatus == 2)
		{ // ReCalcuing
			SendWzInitStatus(2);
			gEvmInitStatus = -1;
			gEvmStartingStatus = EWZ_STATE_STILL;
		}
		else if (gEvmInitStatus == 1)
		{ // Reset OK
			SendWzInitStatus(1);
			gEvmInitStatus = -1;
		}
		else  if( gEvmInitStatus == 5){ //Key Event
			SendWzInitStatus(5);
			gEvmInitStatus = -1;
		}

		//------------ 1 Sec perior -------------
		if (sCount % 2 != 0)
			continue;

		// if( sCount%12 == 0)
		// {	//---- TEST ---- Send Start
		// 	SendWzStartNotify(2);
		// }
		// else if(gbStatisticStat == 1)
		// {	//---- TEST ---- Send Result
		// 	SendWzResultNotify();
		// }
		// else if( sCount%8 == 0)
		// {	//---- TEST ---- Send Init Status
		// 	SendWzInitStatus(1);
		// }
		// else if( sCount%6 == 0)
		// {	//---- TEST ---fs- Request stWzParamDataReq
		// 	SendWzParamRequest();
		// }
	}
}

//===================================================================
//	Send Msg Event/Data

void SendWzStartNotify(int32_t status)
{ //---- TEST ---- Send Init Status
	LogPrintf("//------------------------------------------------------------------------------ DBG ----:  Start(%d) ----\r\n", status);
	stWzStartingNotify packetData = {
		.status = status,
		.weight = 1250,
		.battery = 99,
	};
	packetData.weight = gWzParamData.weight;
	SendCommandMsg(CMDID_START, &packetData, sizeof(stWzStartingNotify), 0);
}

void SendWzInitStatus(int32_t status)
{ //---- TEST ---- Send Init Status
	LogPrintf("//------------------------------------------------------------------------------ DBG ----:  Init(%d) ----\r\n", status);
	stWzInitStatusNotify packetData = {
		.status = status,
		.weight = 1250,
		.battery = 99,
	};
	packetData.weight = gWzParamData.weight;
	SendCommandMsg(CMDID_INIT, &packetData, sizeof(stWzInitStatusNotify), 0);
}

void SendWzParamRequest(void)
{ //---- TEST ---- Request stWzParamDataReq
	LogPrintf("//------------------------------------------------------------------------------ DBG ----:  ParamRequest ----\r\n");
	stWzParamDataReq packetData = {
		.method = 100,
	};
	SendCommandMsg(CMDID_PARAM, &packetData, sizeof(stWzParamDataReq), 0);
}

void SendWzSpeedStatus(int32_t speed)
{ //---- TEST ---- Send RunStatus
	LogPrintf("//------------------------------------------------------------------------------. DBG .... Speed(%.02f) ....\r\n", (float)getCurrentSpeed() / 10000.0);
	stWzRunStatus packetData = {
		.speed = (speed % 5) * 10000,
		.curAccel = speed,
	};
	packetData.speed = getCurrentSpeed();
	SendCommandMsg(CMDID_STATU, &packetData, sizeof(stWzRunStatus), 0);
}

void SendWzResultNotify(void)
{ //---- TEST ---- Send Result
	LogPrintf("//------------------------------------------------------------------------------ DBG ----:  Send Result ----\r\n");
	stWzEndResultNotify packetData = {
		.status = 0,
		.weight = 1250,
		.battery = 96,
		.speed = 16000,
		.A95VibX = 125,
		.A95VibY = 125,
		.A95VibZ = 125,
	};
	if (1)
	{
		packetData.status = 10;
		packetData.weight = gWzParamData.weight;
		packetData.speed = EvmStatistics.MaxSpeed;
		packetData.reserv1 = EvmStatistics.LiftHeight;
		packetData.A95VibX = EvmStatistics.A95VibX;
		packetData.A95VibY = EvmStatistics.A95VibY;
		packetData.A95VibZ = EvmStatistics.A95VibZ;
	}
	LogPrintf("//------------------------------------------------------------------------------ Send Result----\r\n");
	SendCommandMsg(CMDID_RESUT, (int8_t *)&packetData, sizeof(stWzEndResultNotify), 0);
}

//#include "includes.h"
#include "gpio.h"
#include "EwzTimeEvent.h"

unsigned char gDoorPushSen = 0;
unsigned char gDoorPushSenLast = 0;

void Task_IOEventTask(void)
{
	uint32_t err;

	sleep(1);
	struct pollfd fds[1];
	int gpio_fd1, gpio_fd2, gpio_fd3, gpio_fd4;
	unsigned char buffer[64];

	gpio_export(PUSH_SWITCH);
	gpio_set_direction(PUSH_SWITCH, GPIO_INPUT);
	gpio_set_edge(PUSH_SWITCH, BOTH);
	gpio_fd3 = gpio_fd_open(PUSH_SWITCH);
	fds[0].fd = gpio_fd3;
	fds[0].events = POLLPRI;
	gDoorPushSen = gpio_read(PUSH_SWITCH); // Alarm
	gDoorPushSenLast = gDoorPushSen;

	while (1)
	{
		LogPrintf("################## PushSensor: IO:%d\n", gpio_read(PUSH_SWITCH));
		{
			if (poll(fds, 1, 3000) == -1)
			{
				LogPrintf("PUSH_SWITCH: POLL FAIL!\n");
				// return -1;
			}
			else
			{
				if (fds[0].revents & POLLPRI)
				{
					read(gpio_fd3, buffer, 64);
					if (lseek(gpio_fd3, 0, SEEK_SET) == -1)
					{
						LogPrintf("PushSensor: lseek failed!\n");
					}

					usleep(50000);
					gDoorPushSen = (gpio_read(PUSH_SWITCH));
					LogPrintf("PushButton Event: 0x%x IO=%d\n", fds[0].revents, gDoorPushSen);
					if (gDoorPushSen != gDoorPushSenLast)
					{
						gDoorPushSenLast = gDoorPushSen;
						if (gDoorPushSen)
						{
							gTimerPushRelease = TIMEOUT_PUSHBUTTON_RELEASE;
							LogPrintf("Start PushButton TIMER: Event 0x%x IO=%d\n", fds[0].revents, gDoorPushSen);
						}
					}
					continue;
				}
			}
		}
	}
}
